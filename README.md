# supermall
src目录结构说明
    assets                  // 静态资源
        css
        imgs
    common                  // 存放js文件, 如 const.js  utils.js  mixin.js
    components              // vue公共组件
    network                 // 网络通信
    pages                   // vue页面


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
