import Vue from 'vue'
import Router from 'vue-router'
const Home = () => import('./pages/home/Home')
const ShopCart = () => import('./pages/shopcart/ShopCart')
const CateGory = () => import('./pages/category/CateGory')
const Profile = () => import('./pages/profile/Profile')
const Detail = () => import('./pages/detail/Detail')

Vue.use(Router)
const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'home',
    component: Home
  },
  {
    path: '/shopcart',
    name: 'shopcart',
    component: ShopCart
  },
  {
    path: '/category',
    name: 'category',
    component: CateGory
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile
  },
  {
    path: '/detail/:iid',
    name: 'detail',
    component: Detail
  }
]

export default new Router({
  mode: 'history',
  routes
})
